//Emmanuelle Lin 2239468
package linearalgebra;
import java.lang.Math;

public class Vector3d {
    private double x;
    private double y;
    private double z;
    
    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }

    public double magnitude(){
        double magnitude = Math.pow(getX(),2) + Math.pow(getY(),2) + Math.pow(getZ(),2);
        return Math.sqrt(magnitude);
    }

    public double dotProduct(Vector3d anotherVector){
        return (this.x * anotherVector.getX()) + (this.y * anotherVector.getY()) + (this.z * anotherVector.getZ());
    }

    public Vector3d add(Vector3d anotherVector){
        return new Vector3d(this.x + anotherVector.getX(), this.y + anotherVector.getY(), this.z + anotherVector.getZ());
    }

}
