//Emmanuelle Lin 2239468
package linearalgebra;
import static org.junit.Assert.*;
import org.junit.Test;

public class Vector3dTests {
  
    //testing the get methods success
    @Test
    public void getX_return1(){
        Vector3d testVector = new Vector3d(1,2,3);
        assertEquals(1, testVector.getX(), 0.0001);
    }
    @Test
    public void getY_return2(){
        Vector3d testVector = new Vector3d(1,2,3);
        assertEquals(2, testVector.getY(), 0.0001);
    }
    @Test
    public void getZ_return3(){
        Vector3d testVector = new Vector3d(1,2,3);
        assertEquals(3, testVector.getZ(), 0.0001);
    }

    //testing magnitude method
    @Test
    public void magnitude_return7(){
        Vector3d testVector = new Vector3d(2,3,6);
        assertEquals(7, testVector.magnitude(), 0.0001);
    }

    //testing dotProduct method
    @Test
    public void dotProduct_returns20(){
        Vector3d testVector = new Vector3d(1,2,3);
        Vector3d testVector2 = new Vector3d(2,3,4);
        double result = testVector.dotProduct(testVector2);
        assertEquals(20, result, 0.0001);
    }

    //testing add method
    @Test
    public void add_return3_5_7(){
        Vector3d testVector = new Vector3d(1,2,3);
        Vector3d testVector2 = new Vector3d(2,3,4);
        assertEquals(3, testVector.add(testVector2).getX(), 0.0001);
        assertEquals(5, testVector.add(testVector2).getY(), 0.0001);
        assertEquals(7, testVector.add(testVector2).getZ(), 0.0001);
    }

}
